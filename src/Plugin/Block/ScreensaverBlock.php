<?php

namespace Drupal\screensaver\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ScreensaverBlock' block.
 *
 * @Block(
 *  id = "screensaver_block",
 *  admin_label = @Translation("Screensaver block"),
 * )
 */
class ScreensaverBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected $configFactory;

  protected $entityManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->entityManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->configFactory->get('screensaver.screensaverconfig')
      ->getRawData();
    $mediaStorage = $this->entityManager->getStorage('media');
    $fileStorage = $this->entityManager->getStorage('file');


    $screensaver = NULL;
    if ($config['default']['enable-screensaver'] != 1) {
      return [];
    }
    elseif ($config['default']['scheduled-screensaver'] != 1) {
      $screensaver = $fileStorage->load($mediaStorage->load($config['default']['default-screensaver'])->field_media_image->target_id)
        ->createFileUrl();
    }
    else {
      $today = strtolower(date('l'));
      if (empty($config[$today])) {
        $screensaver = $fileStorage->load($mediaStorage->load($config['default']['default-screensaver'])->field_media_image->target_id)
          ->createFileUrl();
      }
      else {
        $actual_time = DrupalDateTime::createFromTimestamp(strtotime(date('H:m:s')));

        for ($i = 0; $i < $config[$today][$today . '_number']; $i++) {
          $startTime = DrupalDateTime::createFromTimestamp(strtotime($config[$today][$today . '-time-start-' . $i]));
          $endTime = DrupalDateTime::createFromTimestamp(strtotime($config[$today][$today . '-time-end-' . $i]));

          if ($actual_time >= $startTime && $actual_time <= $endTime) {
            $screensaver = $fileStorage->load($mediaStorage->load($config[$today][$today . '-screensaver-' . $i])->field_media_image->target_id)
              ->createFileUrl();
          }
        }

        if (empty($screensaver)) {
          $screensaver = $fileStorage->load($mediaStorage->load($config['default']['default-screensaver'])->field_media_image->target_id)
            ->createFileUrl();
        }
      }
    }

    $build = [];
    $build['#theme'] = 'screensaver_block';
    $build['#attached'] = [
      'library' => [
        'screensaver/screensaver',
      ],
    ];

    $build['#attached']['drupalSettings']['timeout_screensaver'] = ($config['default']['timeout-screensaver']*1000);
    $build['#attached']['drupalSettings']['screensaver'] = $screensaver;

    if ($config['default']['redirect'] == 1){
      $build['#attached']['drupalSettings']['redirect'] = $config['default']['redirect'];
      $build['#attached']['drupalSettings']['url_redirect'] = Url::fromUserInput($config['default']['url-redirect'])->getInternalPath();
      $build['#attached']['drupalSettings']['timeout_redirect'] = ($config['default']['timeout-redirect']*1000);
    }

    return $build;
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheMaxAge() {
    return 300;
  }


}
