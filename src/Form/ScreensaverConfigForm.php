<?php

namespace Drupal\screensaver\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ScreensaverConfigForm.
 */
class ScreensaverConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The configuration day.
   *
   * @var string
   */
  protected $day;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->day = '';
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'screensaver.screensaverconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'screensaver_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('screensaver.screensaverconfig')->getRawData()[$this->day];

    if (empty($config)){
      $config = [];
    }

    if (!$this->config('screensaver.screensaverconfig.default')->get('scheduled-screensaver')){
      $this->messenger()->addWarning($this->t('Warning: Scheduled screensaver is disabled in default configuration.'));
    }

    if (!empty($form_state->get($this->day . '_number'))) {
      $numValues = $form_state->get($this->day . '_number');
      $config[$this->day . '_number'] =  $numValues;
    }
    elseif (is_numeric($config[$this->day . '_number'])) {
      $numValues =  $config[$this->day . '_number'];
      $form_state->set($this->day . '_number', $numValues);
    }
    else {
      $numValues = 1;
      $form_state->set($this->day . '_number', $numValues);
      $config[$this->day . '_number'] =  $numValues;
    }

    $this->config('screensaver.screensaverconfig')->set($this->day, $config)->save();

    $form['details'] = [
      '#type' => 'container',
      '#title' => $this->t('Monday screensaver configuration'),
      '#prefix' => '<div id="' . $this->day . '-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $numValues; $i++) {
      $form['details'][$i] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t(ucfirst($this->day))->render() . ' ' . ($i+1),
      ];


      $form['details'][$i]['time-start-' . $i] = [
        '#type' => 'datetime',
        '#title' => $this->t('START TIME'),
        '#date_date_element' => 'none',
        '#date_time_element' => 'time',
        '#date_time_format' => 'H:i:s',
        '#required' => TRUE,
        '#default_value' => !empty($config[$this->day . '-time-start-' . $i]) ? DrupalDateTime::createFromTimestamp(strtotime($config[$this->day . '-time-start-' . $i])) : NULL,
      ];

      $form['details'][$i]['time-end-' . $i] = [
        '#type' => 'datetime',
        '#title' => $this->t('END TIME'),
        '#date_date_element' => 'none',
        '#date_time_element' => 'time',
        '#date_time_format' => 'H:i:s',
        '#required' => TRUE,
        '#default_value' => !empty($config[$this->day . '-time-end-' . $i]) ? DrupalDateTime::createFromTimestamp(strtotime($config[$this->day . '-time-end-' . $i])) : NULL,
      ];

      $form['details'][$i]['screensaver-' . $i] = [
        '#type' => 'media_library',
        '#allowed_bundles' => ['image'],
        '#limit_validation_errors' => [],
        '#required' => TRUE,
        '#title' => $this->t('Screensaver image'),
        '#description' => $this->t('Upload or select an image for the screensaver.'),
        '#default_value' => $config[$this->day . '-screensaver-' . $i],
        '#suffix' => '<hr>',
      ];
    }

    $form['details'][$i]['actions'] = [
      '#type' => 'actions',
    ];

    $form['details'][$i]['actions']['add_more_' . $this->day] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#name' => $this->day,
      '#submit' => ['::addOne'],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => $this->day . '-wrapper',
      ],
    ];
    // If there is more than one name, add the remove button.
    if ($numValues > 1) {
      $form['details'][$i]['actions']['remove_one_' . $this->day] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove last'),
        '#name' => $this->day,
        '#limit_validation_errors' => [],
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => $this->day . '-wrapper',
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['details'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $add_button = $form_state->get($this->day. '_number') + 1;
    $form_state->set($this->day . '_number', $add_button);
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $number_elements = $form_state->get($this->day . '_number');

    if ($number_elements > 1) {
      $remove_button = $number_elements - 1;
      $form_state->set($this->day . '_number', $remove_button);
    }
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#limit_validation_errors'] === FALSE) {
      foreach ($form_state->getValues() as $key => $value) {
        if (str_contains($key, 'time-start')) {
          $timeStart[] = $value instanceof DrupalDateTime ? $value->format('TH:m:s') : NULL;
        }
        elseif (str_contains($key, 'time-end')) {
          $timeEnd[] = $value instanceof DrupalDateTime ? $value->format('TH:m:s') : NULL;
        }
      }

      $sameElement = FALSE;
      $sameStart = FALSE;
      $sameEnd = FALSE;
      $endBeforeStart = FALSE;

      for ($x = 0; $x < count($timeStart); $x++) {
        if ($timeStart[$x] == $timeEnd[$x] && !empty($timeStart[$x]) && !empty($timeEnd[$x])) {
          $form_state->setErrorByName('time-start-' . $x);
          $form_state->setErrorByName('time-end-' . $x);
          $sameElement = TRUE;
        }
              elseif (strtotime($timeEnd[$x]) < strtotime($timeStart[$x])){
                $form_state->setErrorByName('time-start-' . $x);
                $form_state->setErrorByName('time-end-' . $x);
                $endBeforeStart = TRUE;
              }
        for ($y = 0; $y < count($timeStart); $y++) {
          if ($x !== $y) {
            if ($timeStart[$x] == $timeStart[$y] && !empty($timeStart[$x]) && !empty($timeStart[$y])) {
              $form_state->setErrorByName('time-start-' . $x);
              $sameStart = TRUE;
              //            $form_state->setErrorByName('time-start-' . $x, $this->t("Monday")->render() . " " . ($x+1) . " " . $this->t('has the same START TIME as Monday')
              //                ->render() . " " . ($y+1));
            }

            if ($timeEnd[$x] == $timeEnd[$y] && !empty($timeEnd[$x]) && !empty($timeEnd[$y])) {
              $form_state->setErrorByName('time-end-' . $x);
              $sameEnd = TRUE;
              //            $form_state->setErrorByName('time-end-' . $x, $this->t("Monday")->render() . " " . ($x+1) . " " . $this->t('has the same END TIME as Monday')
              //                ->render() . " " . ($y+1));
            }
          }
        }
      }

      if ($sameElement) {
        $this->messenger()
          ->addError($this->t('One or more form elements has the same START and END TIME.'));
      }
      if ($sameStart) {
        $this->messenger()
          ->addError($this->t('More than one form element has the same START time.'));
      }
      if ($sameEnd) {
        $this->messenger()
          ->addError($this->t('More than one form element has the same END time.'));
      }
      if ($endBeforeStart) {
        $this->messenger()
          ->addError($this->t('END time cannot be earlier than START time.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $formValues = $form_state->getValues();
    $numElements = $this->config('screensaver.screensaverconfig')->get($this->day)[$this->day . '_number'];
    $config = [];

    $this->config('screensaver.screensaverconfig')->set($this->day, []);

    $config[$this->day . '_number'] = $numElements;

    foreach ($formValues as $key => $value) {
      if ($value instanceof DrupalDateTime) {
        $value = $value->format($value::FORMAT);
        $config[$this->day . '-' . $key] = $value;
      }
      elseif (str_contains($key, 'screensaver-')) {
        $config[$this->day . '-' . $key] = $value;
      }
    }
    $this->config('screensaver.screensaverconfig')->set($this->day, $config)->save();
  }

}
