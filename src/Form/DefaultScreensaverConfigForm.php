<?php

namespace Drupal\screensaver\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DefaultScreensaverConfigForm.
 */
class DefaultScreensaverConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_screensaver_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'screensaver.screensaverconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->day = 'default';
    $config = $this->config('screensaver.screensaverconfig')->getRawData()[$this->day];

    $form['enable-screensaver'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable screensaver.'),
      '#default_value' => $config['enable-screensaver'],
    ];

    $form['scheduled-screensaver'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable scheduled screensaver.'),
      '#default_value' => $config['scheduled-screensaver'],
      '#states' => [
        'unchecked' => [
          ':input[name="enable-screensaver"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['redirect'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable redirect.'),
      '#default_value' => $config['timeout'],
      '#states' => [
        'unchecked' => [
          ':input[name="enable-screensaver"]' =>  ['checked' => FALSE],
        ],
      ],
    ];

    $form['timeout-screensaver'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout to fade screensaver in (in seconds).'),
      '#default_value' => !empty($config['timeout-screensaver']) ? $config['timeout-screensaver'] : 300,
    ];

    $form['url-redirect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Internal url to redirect.'),
      '#default_value' => !empty($config['url-redirect']) ? $config['url-redirect'] : "/",
    ];

    $form['timeout-redirect'] = [
      '#type' => 'number',
      '#title' => $this->t('Timeout to redirect to configured page (in seconds).'),
      '#default_value' => !empty($config['timeout-redirect']) ? $config['timeout-redirect'] : 1200,
    ];

    $form['default-screensaver'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => ['image'],
      '#limit_validation_errors' => [],
      '#title' => $this->t('Default screensaver image'),
      '#description' => $this->t('Upload or select an image for the screensaver.'),
      '#default_value' => $config['default-screensaver'],
      '#suffix' => '<hr>',
    ];

    return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $url_redirect = $form_state->getValue('url-redirect');
    if (!empty($url_redirect)) {
      $user_url = Url::fromUserInput($url_redirect);
      if ($user_url->isExternal()) {
        $form_state->setErrorByName('url-redirect', $this->t('URL must be internal.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = [
      'scheduled-screensaver' => $form_state->getValue('scheduled-screensaver'),
      'enable-screensaver' => $form_state->getValue('enable-screensaver'),
      'default-screensaver' => $form_state->getValue('default-screensaver'),
      'timeout-screensaver' => $form_state->getValue('timeout-screensaver'),
      'timeout-redirect' => $form_state->getValue('timeout-redirect'),
      'redirect' => $form_state->getValue('redirect'),
      'url-redirect' => $form_state->getValue('url-redirect'),
    ];

    $this->config('screensaver.screensaverconfig')->set($this->day, $config)->save();

  }



}
