# Screensaver

This project provides a highly configurable screensaver for Drupal 8 and 9.

The main features of the module is a screensaver that pops on the screen after
a configured timeout, and can redirect the user after an also configurable
timeout to, for example, the front page of the website.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/screensaver).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/screensaver).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The configuration form is under

`Configuration -> Media -> Screensaver`


## Maintainers

- Cristian García - [clgarciab](https://www.drupal.org/u/clgarciab)
- Juan Natera - [jncruces](https://www.drupal.org/u/jncruces)
