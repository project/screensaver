(function($) {
    Drupal.behaviors.myBehavior = {
        attach: function (context, settings) {

            window.addEventListener('load', function(){
                let s_saver_timeout = settings['timeout_screensaver'];
                let background_url = "../../../.." + settings['screensaver'];
                let s_redirect = settings['redirect'];
                let url_redirect;
                let timeout_redirect;
                if (s_redirect == 1){
                    url_redirect = settings['url_redirect'];
                    timeout_redirect = settings['timeout_redirect'];
                }

                let div = "<div id='screensaver' style='background-image: url(" + background_url + ")'></div>";

                let s_saver = setTimeout(function (){
                    $('body').hide();
                    $('#screensaver').fadeIn(900);
                }, s_saver_timeout);

                let redirect = setTimeout(function (){
                    window.location.href = url_redirect;
                }, timeout_redirect);

                $('html').once().append(div);
                $('html').mousemove(function (){
                    clearTimeout(s_saver);
                    clearTimeout(redirect);

                    $('#screensaver').hide();
                    $('body').show();

                    s_saver = setTimeout(function (){
                        $('body').hide();
                        $('#screensaver').fadeIn(900);
                    }, s_saver_timeout);

                    if (s_redirect == 1){
                        redirect = setTimeout(function (){
                            window.location.href = url_redirect;
                        }, timeout_redirect);
                    }
                });

                $('html').click(function() {
                    clearTimeout(s_saver);
                    clearTimeout(redirect);

                    $('#screensaver').hide();
                    $('body').show();

                    s_saver = setTimeout(function (){
                        $('body').hide();
                        $('#screensaver').fadeIn(900);
                    }, s_saver_timeout);

                    if (s_redirect == 1){
                        redirect = setTimeout(function (){
                            window.location.href = url_redirect;
                        }, timeout_redirect);
                    }
                });
            });

        }
    };
})(jQuery);


